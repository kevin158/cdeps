/**
 * @brief From https://github.com/hongliuliao/simple-log
 * @note Modified by Kevin.XU
 */

#ifndef SIMPLE_LOG_H
#define SIMPLE_LOG_H

#include <cstring>

#include <string>
#include <fstream>
#include <utility>
#include <vector>

#include <dirent.h>
#include <sys/time.h>
#include <unistd.h>
#include <pthread.h>

/**
 * @brief 日志级别定义
 */
const int ERROR_LEVEL = 1;
const int WARN_LEVEL = 2;
const int INFO_LEVEL = 3;
const int DEBUG_LEVEL = 4;

/**
 * @brief 日志级别
 */
extern int log_level;

/**
 * @brief 日志中显示的文件名最大长度
 */
#define RESERVED_FILE_LEN  28

/**
 * @brief 各种日志输出宏
 */
#define LOG_ERROR(format, args...) \
    if(log_level >= ERROR_LEVEL) { \
		log_error("%u-%u %s %s(%d): " format, getpid(), pthread_self(), "ERROR", format_file_path(__FILE__,RESERVED_FILE_LEN), __LINE__, ##args); \
    }

#define LOG_WARN(format, args...) \
    if(log_level >= WARN_LEVEL) { \
		log_warn("%u-%u %s %s(%d): " format, getpid(), pthread_self(), "WARN", format_file_path(__FILE__,RESERVED_FILE_LEN), __LINE__, ##args); \
    }

#define LOG_INFO(format, args...) \
    if(log_level >= INFO_LEVEL) { \
		log_info("%u-%u %s %s(%d): " format, getpid(), pthread_self(), "INFO", format_file_path(__FILE__,RESERVED_FILE_LEN), __LINE__, ##args); \
    }

#define LOG_DEBUG(format, args...) \
    if(log_level >= DEBUG_LEVEL) { \
		log_debug("%u-%u %s %s(%d): " format, getpid(), pthread_self(), "DEBUG", format_file_path(__FILE__,RESERVED_FILE_LEN), __LINE__, ##args); \
    }

void log_error(const char *format, ...);
void log_warn(const char *format, ...);
void log_info(const char *format, ...);
void log_debug(const char *format, ...);

/**
 * 递归创建目录
 * @param sPathName
 * @return
 */
int create_dir(const char * sPathName);

/**
 * 查找目录下的文件
 * @param dir
 * @param files
 * @param tail
 */
void scandir(const char *dir, std::vector<std::string> &files, const std::string &tail = "");

/**
 * 缩减文件名路径的长度，便于在日志中输出所在文件的名字
 * @param path
 * @param reserved_len
 * @return
 */
char * format_file_path(const char * path, size_t reserved_len);

/**
 * 返回日期的格式串，%04d-%02d-%02d %02d:%02d:%02d.%03d
 * @param tv
 * @return
 */
std::string _get_show_time(timeval tv);

/**
 * 初始化日志系统
 * @param configFilePath
 * @return
 */
int log_init(const std::string& configFilePath = "./simple_log.conf");

/**
 * 设置日志级别
 * @param level
 */
void set_log_level(const char *level);

/**
 * @brief 日志输出类
 */
class FileAppender {
public:
    FileAppender();
    ~FileAppender();
    /**
     * 初始化日志输出文件
     * @param dir
     * @param log_file
     * @return
     */
    int init(const std::string& dir, const std::string& log_file);
    /**
     * 是否被初始化
     * @return
     */
    bool is_inited();
    /**
     * 写日志
     * @param format
     * @param ap
     * @return
     */
    int write_log(const char *format, va_list ap);
    /**
     * 日志切换
     * @param tv
     * @param tz
     * @return
     */
    int shift_file_if_need(struct timeval tv, struct timezone tz);
    /**
     * 删除老日志文件
     * @param tv
     * @return
     */
    int delete_old_log(timeval tv);
    /**
     * 设置保留天数
     * @param rd
     */
    void set_retain_day(int rd);
    /**
     * 构造文件名
     * @param buffer
     * @param len
     * @param tm
     */
    std::string format_file_name(struct tm *tm);
    /**
     * 清除历史上的老文件
     */
    void clear_old_logs();
private:
    std::fstream _fs;
    std::string _log_file;
    std::string _log_dir;
    std::string _log_file_path;
    long _last_sec;
    bool _is_inited;
    int _retain_day;
    pthread_mutex_t writelock;
};

#endif

#!/bin/bash

######README
#This script will install 3rd party open source tool/lib into $HOME/openSourceLib
#if you want to change installation destination path, please modify the var [installTo]
#and ensure you have write permission of that path folder
#
#useage: sh [/script parrent folder path/]compile3rdResources.sh <software1 software2 ...>
#sample: 
#   Invoke script by full path from /:
#       sh /home/..../compile3rdResources.sh <software1 software2 ...>
#   Invoke script by go to go to script folder and run script:
#       sh compile3rdResources.sh <software1 software2 ...>
#   Invoke script by related folder path and run the script:
#       sh ../scripts/compile3rdResources.sh <software1 software2 ...>
#note:
#   This script will update installed module in file $HOME/openSourceLib/history.log
#   If the module you want to install didn't exist in the file "$HOME/openSourceLib/history.log", the module will be installed
#   If the module you want to install has been existed in the file, installation will be ignored
#   so, if you want to re-install a 3rd party open source tool/lib, before re-install please manually delete the module in the file and clean corresponding libs in $HOME/openSourceLib/      


###initial global vars
invokeSh="$0"
shPath="${invokeSh%/*}"
moduleParentDirname="deps"              ##this constand dirname decided by git code tree
modulePath="$shPath/../$moduleParentDirname"
moduleNum="$#"
modules="$@"
buildFolder="buildAuto"                 ##I create this folder to use to run cmake .. make and make install
installTo="$HOME/openSourceLib"         ##use to save libs of the moudule you installed
bashrc="$HOME/.bashrc"
installHistory="$installTo/history.log" ##use to record what modules has been installed



##ENV VAR
LD_LIBRARY_PATH="$LD_LIBRARY_PATH"
C_INCLUDE_PATH="$C_INCLUDE_PATH"
CPLUS_INCLUDE_PATH="$CPLUS_INCLUDE_PATH"
PATH="$PATH"



useage() {
    if [ 0 -eq $moduleNum ];then
        echo 
        echo "Useage: sh $invokeSh <software1 software2 ...>"
        echo "You did NOT specify any software, please append at least one software to be installed"
        echo
        exit 1
    fi
    echo "will compile/install [$modules] into [$installTo], total $moduleNum"
}

##check if the module was installed
#if installed, return true; else return false
checkModuleIsInstalled() {
    local module="$1"

    grep "$module" "$installHistory"
    if [ 0 -eq $? ];then
        echo "===[$module] had been installed"
        return 0
    else
        return 1
    fi
}

##$1---module path: the path folder inlcude modules, such as /Users/ali-132547n/jason/workspace/git/cdeps/deps
##$2---module name: the name of 3rd party module name, such as googletest
compileModule() {
    local modulePath="${1}"
    local module="${2}"
    local libPath="$installTo"
    local compilePath="$modulePath/$module"
    local pathOri="$(pwd)"

    checkModuleIsInstalled "$module"
    if [ 0 -eq $? ];then
        echo "===No need to install [$module]"
        return
    fi
    echo "===start to intall third party open source: $module"
    if [ ! -d "$compilePath" ];then
        echo "===FAIL, no such module. Module path: $compilePath"
        exit 1
    fi

    cd "$compilePath"

    [ ! -e "$buildFolder" ] && mkdir "$buildFolder"
    cd "$buildFolder" 
    rm -rf *
    [ ! -d "$libPath" ] && mkdir -p "$libPath"

    if [ -z "$CTC_ATOM_HOME" ];then
        cmake -DCMAKE_INSTALL_PREFIX="$libPath" .. && make && make install
    else
        echo "===This is ATOM platform"
        echo "CTC_ATOM_HOME = $CTC_ATOM_HOME"
        cmake -DCMAKE_INSTALL_PREFIX="$libPath" -DCMAKE_TOOLCHAIN_FILE=../../../pepper-toolchain.cmake .. && make && make install
    fi

    if [ $? -eq 0 ];then
        echo "===successfully intalled [$module]"
        ##update install history log
        echo "$module" >> "$installHistory"
        LD_LIBRARY_PATH="$libPath/lib:$LD_LIBRARY_PATH"
        C_INCLUDE_PATH="$libPath/include/:$C_INCLUDE_PATH"
        CPLUS_INCLUDE_PATH="$libPath/include/:$CPLUS_INCLUDE_PATH"
        PATH="$libPath/bin:$PATH"
    else
        echo "===FAIL, install $module failure"
        exit 1
    fi

    ##go to original path for next cycle running correctly
    cd "$pathOri"

}

###no need to export env now, we used cmake flag to enable installed libs
#cmake -DCMAKE_INCLUDE_PATH=~/openSourceLib/include -DCMAKE_LIBRARY_PATH=~/openSourceLib/lib ..
exportEnv() {
    echo "===start export env var for modules: $modules"
    echo "" >> $bashrc
    echo "#third party open source: $modules export setting" >> $bashrc
    echo "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH" >> $bashrc
    echo "export C_INCLUDE_PATH=$C_INCLUDE_PATH" >> $bashrc
    echo "export CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH" >> $bashrc
    echo "export PATH=$PATH" >> $bashrc
    source $bashrc
}

##handle all kinds of invoke path
##1. ./xxx.sh or sh xxx.sh
##2. ./scripts/xxx.sh or sh scripts/xxx.sh or sh ../../scripts/xxx.sh
##3. /home/..../xxx.sh or sh /home/..../xxx.sh
getModulePath() {
    if [ "$invokeSh" = "$shPath" ];then
        #current pwd is /.../scripts/
        modulePath="$(pwd)/../$moduleParentDirname"
    else
        ##pick up related path of script
        modulePath="$shPath/../$moduleParentDirname"
    fi
}

runInstall() {
    local module

    getModulePath
    for module in $modules
    do
        compileModule "$modulePath" "$module"
    done 
}


useage
runInstall



//
// Created by xuhuahai on 2017/5/24.
//

#include <simple_log.h>
#include <string>
#include <cassert>

#include <unistd.h>

void log_initTest(){
    log_init("/Users/xuhuahai/projects/cdeps/scripts/simple_log.conf");
    LOG_DEBUG("11111");
    int count = 1000;
    while(count-->0){
        sleep(1);
        LOG_INFO(" ----  %d",count);
    }
    LOG_ERROR("SOME ERROR");
}


int main(int argc, char** argv){
    char buff[256];
    getcwd(buff,sizeof(buff));
    printf("cwd=%s\n",buff);
    log_initTest();
    return 0;
}
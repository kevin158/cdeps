cdeps
====

为了管理公共依赖的第三方库，建立这个工程。

<br><br>


#功能介绍

* cppjson
* simplelog
* re2
* googletest

<br><br>

#开发成员

Kevin :  421093703@qq.com

<br><br>

#手动本地编译

mkdir build

cd build

cmake ..

make 

make install


<br><br>

#交叉编译


手动安装

从服务器100.81.153.60:/workspace/haisheng.yhs/ctc-linux64-atom-2.5.2.74.zip下载到本地

unzip ctc-linux64-atom-2.5.2.74.zip -d /root/

export CTC_ATOM_HOME=/root/ctc-linux64-atom-2.5.2.74

mkdir build

cd build

cmake -DCMAKE_INSTALL_PREFIX=/root/local -DCMAKE_TOOLCHAIN_FILE=pepper-toolchain.cmake ..

make 

make install

<br><br>


